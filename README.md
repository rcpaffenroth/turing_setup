# Introduction

This repository contains several scripts to help you get started running Python on Turing. I have attempted to document the scripts so please look at the scripts themselves for more information. Here are the various directories of interest.

## advanced

This is not for most people, and contains more advanced scripts (e.g., for compilining Pytorch from source)

## jupyter_notebooks

Some scripts for running Jupyter notebooks on Turing. There are examples for Pytorch and Scilkit-learn

## python

Some scripts for running Python on Turing. There are examples for Pytorch and Scilkit-learn

## scripts

Some scripts for using Turing more generally. For example, for getting an interactive node.

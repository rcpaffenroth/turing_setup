#! /bin/bash

CONDAROOT=/work/rcpaffenroth/opt/anaconda3
CONDABIN=$CONDAROOT/bin

# This should match the version in the conda installed in the setup_python.sh
module load cuda/11.0.2
# pytorch 1.8.1 seems to like gcc >= 5 and <=9
module load cmake/gcc-8.2.0

# Based on https://github.com/pytorch/pytorch#from-source

# # Install the pytorch prerequisites
# $CONDABIN/conda install -y numpy ninja pyyaml mkl mkl-include setuptools cmake cffi typing_extensions future six requests dataclasses
# # Install a recent version of cuda
# $CONDABIN/conda install -y -c pytorch magma-cuda110

# # Get the pytorch source
# git clone --recursive https://github.com/pytorch/pytorch
cd pytorch
# # We fix at this version to make this work consistently
# git checkout v1.8.1
# if you are updating an existing checkout
git submodule sync
git submodule update --init --recursive

export CMAKE_PREFIX_PATH=${CONDA_PREFIX:-"$(dirname $(which conda))/../"}
# This is the magic!  You need the correct compute capabilities for the Turing
# GPU nodes. 
# The full list is here:  https://developer.nvidia.com/cuda-gpus
# For us:
# K20     3.5
# K40     3.5
# K80     3.7
# P100    6.0
# V100    7.0
# T4      7.5
# A100    8.0

# Also something to we aware of:
# nvcc warning : The 'compute_35', 'compute_37', 'compute_50', 'sm_35', 'sm_37' and 'sm_50' architectures are deprecated, and may be removed in a future release (Use -Wno-deprecated-gpu-targets to suppress warning).

export TORCH_CUDA_ARCH_LIST_VAR="3.5;3.7+PTX;5.0;6.0;6.1;7.0;7.5;8.0"

python setup.py install

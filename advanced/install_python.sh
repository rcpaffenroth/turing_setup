#! /bin/bash
echo "This script sets up a lot of environment variables so remember that you need to 'source python.sh'\n"
echo "Instead of just running the script directly.\n\n\n"

echo "NOTE: This script assumes you shell is bash (which is the default on turing)."
echo "If that is not the case then you likely don't need this script.\n\n\n"

CONDAURL=https://repo.anaconda.com/archive/Anaconda3-2021.04-Linux-x86_64.sh
CONDANAME=Anaconda3-2021.04-Linux-x86_64.sh
# You can change this to be any directory you like if you want your own custom install.
CONDAROOT=/work/rcpaffenroth/opt/anaconda3
CONDABIN=$CONDAROOT/bin

# Install anaconda if it is not already installed
if [ ! -d $CONDAROOT ] ; then
    wget $CONDAURL
    mkdir -p $HOME/Downloads
    mv $CONDANAME $HOME/Downloads
    bash $HOME/Downloads/$CONDANAME -b -p $CONDAROOT
fi

echo "Do you want to setup your .bashrc to default to this python installation? ('yes'/'no')"
echo "If you are just getting started you likely want to say 'yes'"
read MAKEDEFAULT

if [ $MAKEDEFAULT == "yes" ]; then
  echo "Making $CONDAROOT the default"
  $HOME/miniconda3/bin/conda init
  source ~/.bashrc
  conda activate base
else
  echo "Leaving the default unchanged"
fi

echo "Install the pytorch deep learning library and other associated software"
echo "Installs both GPU and CPU  version"

# We install mamba since conda is very slow for this
conda install -y -c conda-forge mamba

# There is a sorry state of affairs with ace/turing and pytorch.  Specifically, the K20 and K40 cards are being deprecated.   
# As of 5/12/2021 they are not supported by current versions pytorch, though they are still supported by recent versions of CUDA.
# Accordingly, while it is theoretically possible to setup a single python installation that works across all
# turing/ace GPUs, it is non-trivial.  Accordingly, this is all setup only setup for the K80, P100, V100, T4, and A100 cards.
mamba install -y -c pytorch -c comet_ml -c conda-forge pytorch=1.8 torchvision=0.9 cudatoolkit=11.1 papermill=2.3 comet_ml=3.9 pytorch-lightning=1.3











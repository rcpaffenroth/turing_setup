#! /bin/sh
# Other common command line options
# To get larger memory
# --mem=16gb

# Get a node with just a CPU
#srun --pty bash    
# Get a node with a "stable" GPU
#srun --pty --gres=gpu --constraint="K80|V100|P100|T4" bash
# Get a node is an "old" GPU
#srun --pty --gres=gpu --constraint="K40" bash
# Get a node is an "new" GPU
#srun --pty --gres=gpu --constraint="A100" bash

# Get a specific node
# srun --pty --nodelist=compute-0-25 bash

# Get a specific queue.  The options are 
# short: Jobs lasting less than 24 hours
# long: Jobs lasting less that a week
# srun --pty -p short bash

srun --pty --gres=gpu --constraint="K80|V100|P100|T4" --mem=16gb bash


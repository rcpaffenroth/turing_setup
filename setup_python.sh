#! /bin/bash
echo "This script sets up a lot of environment variables so remember that you need to 'source setup_python.sh'\n"
echo "Instead of just running the script directly.\n\n\n"

echo "NOTE: This script assumes you shell is bash (which is the default on turing)."
echo "If that is not the case then you likely don't need this script.\n\n\n"

# You can change this to be any directory you like if you want your own custom install.
CONDAROOT=/work/rcpaffenroth/opt/anaconda3
CONDABIN=$CONDAROOT/bin

echo "Do you want to setup your .bashrc to default to this python installation? ('yes'/'no')"
echo "If you are just getting started you likely want to say 'yes'"
read MAKEDEFAULT

if [ $MAKEDEFAULT == "yes" ]; then
  echo "Making $CONDAROOT the default"
  $HOME/miniconda3/bin/conda init
  source ~/.bashrc
  conda activate base
else
  echo "Leaving the default unchanged"
fi









